bl_info = {
    "name": "WoodCut",
    "author": "Thr0lu5 <thr0lu5@protonmail.com>",
    "version": (1, 10),
    "blender": (2, 80, 0),
    "location": "View3D > Toolshelf",
    "description": "WoodCut Tools",
    "warning": "",
    "doc_url": "",
    "category": "WoodCut",
}

import bpy
from math import radians
from operator import itemgetter
import os

########## TO DO #########

# Inside Backboard has 15mm + 6mm, Fix Partitions

# Check modular objects contruction

# Build Box and board on cursor location

# partition Fianco with no array and options of hole size

# Labels

# chapa engrossada 30 e 45

# create board in current collection if inside Woodcut Collection 

# Sair do modo de edicao quando calcular

# GET AREA OF BOXES

# Fix location bug on support in 6mm and 18mm

# On box creation, create parent bounding box for size

# Create box gaveta

# Remover MDF do nome dos materiais

# Fix Apply Scale and Center Pivot ERROR

# Separar Build List com nome da arvore Subgrupos das colecoes

##########





### Properties ###
class PG_woodcut(bpy.types.PropertyGroup):

# # Thickness
    thickness_board: bpy.props.EnumProperty(
        name = 'Espessura',
        default = '15',
        items = [('6', '6 mm', ''), ('15', '15 mm', ''), ('18', '18 mm', '')])

# # Board
    board_height: bpy.props.FloatProperty(
        name = 'Altura',
        min = 0.01, max = 2.75, default = 2.75)
    
    board_lenght: bpy.props.FloatProperty(
        name = 'Largura',
        min = 0.01, max = 1.84, default = 1.84)

# # Box
    box_type: bpy.props.EnumProperty(
        name = 'Tipo',
        description = 'Select type of box',
        default = 'basico',
        items = [('basico', 'Basico', ''),
                 ('balcao', 'Balcao', ''),
                 ('cozinha', 'Cozinha', '')]) 

    box_height: bpy.props.FloatProperty(
        name = 'Altura',
        min = 0.1, max = 2.75, default = 0.8)
    
    box_lenght: bpy.props.FloatProperty(
        name = 'Largura',
        min = 0.1, max = 2.75, default = 1)
        
    box_width: bpy.props.FloatProperty(
        name = 'Profundidade',
        min = 0.1, max = 1.84, default = 0.6)    
     
    backboard: bpy.props.EnumProperty(
        name = 'Fundo',
        description = 'Select type Backboard',
        default = 'aberto',
        items = [('aberto', 'Aberto', ''),
                 ('recuado', 'Recuado', ''),
                 ('fora', 'Por fora', '')])
    
    box_partitions: bpy.props.IntProperty(
        name = 'Vãos',
        min = 1, max = 2,
        default = 1)
        
    build_support: bpy.props.BoolProperty(
        name = 'Com suporte?', default = False)
                    
# # Lists
    l_parts = [] # All parts
    l_shaders = [] # Used materials
    l_boxes = [] # Boxes
    l_boards = [] # Boards needed
    l_cutlist = [] # Cut
    l_buildlist = [] # Build
    
# # Calc
    build_visual_map: bpy.props.BoolProperty(
        name = 'Criar mapa visual?', default = False)
    
# # Export
    export_type: bpy.props.EnumProperty(
        name = 'Exportar',
        default = 'todos',
        items = [('todos', 'Todos', ''),
                 ('corte', 'Lista de Corte', ''),
                 ('montagem', 'Lista de Montagem', ''),
                 ('csv', 'Csv', '')])
    
    export_path: bpy.props. StringProperty(
        name = 'Pasta',
        default = '//WoodCut/',
        subtype = 'DIR_PATH')





### METHODS ###
class WC_methods():
    
# # Get chosen thickness
    def F_thickness(self):
        woodcut = bpy.context.scene.woodcut
        
        if woodcut.thickness_board == '6': width = 0.006
        elif woodcut.thickness_board == '15': width = 0.015
        elif woodcut.thickness_board == '18': width = 0.018
        
        return float(width)

# # Create Board
    def F_create_board(self,width, height, lenght,
                       pos_x, pos_y, pos_z, rot_x, rot_y, rot_z,
                       name):
        
        bpy.ops.mesh.primitive_cube_add(
        size = 1, align = 'WORLD', calc_uvs = True,
        scale = (height, lenght, width),
        location = (pos_x, pos_y, pos_z),
        rotation = (rot_x, rot_y, rot_z))

        bpy.context.object.name = name
        bpy.context.object.lock_scale[2] = True
        
# # Create Shader if not and apply
        if bpy.data.materials.find('Branco') == -1:
            S_grain = bpy.data.materials.new(name = 'Branco')
            S_grain.use_nodes = True
        
            SG_principled_node = S_grain.node_tree.nodes.get('Principled BSDF')
            SG_checker_node = S_grain.node_tree.nodes.new('ShaderNodeTexChecker')
            SG_math_node = S_grain.node_tree.nodes.new('ShaderNodeVectorMath')
            SG_coordinate_node = S_grain.node_tree.nodes.new('ShaderNodeTexCoord')
            
            SG_math_node.operation = 'MULTIPLY'
            SG_math_node.inputs[1].default_value = (0,1,0)
             
            S_grain.node_tree.links.new(SG_checker_node.outputs[0], SG_principled_node.inputs[0])       
            S_grain.node_tree.links.new(SG_math_node.outputs[0], SG_checker_node.inputs[0])
            S_grain.node_tree.links.new(SG_coordinate_node.outputs[0], SG_math_node.inputs[0])
    
        bpy.context.object.active_material = bpy.data.materials['Branco']
         
        return{'FINISHED'}

# # Clear Calc # #
    def F_clear_lists(self):
        woodcut = bpy.context.scene.woodcut

        woodcut.l_parts.clear()
        woodcut.l_shaders.clear()
        woodcut.l_boxes.clear()
        woodcut.l_boards.clear()
        woodcut.l_cutlist.clear()
        woodcut.l_buildlist.clear()
        
        if bpy.data.collections.find('ID') != -1:
            c_id = bpy.data.collections.get('ID')
            c_del = bpy.data.collections['WoodCut'].get('ID')
            
            for ob in c_id.objects:
                 bpy.data.objects.remove(ob, do_unlink = True)
            
            bpy.data.collections.remove(c_id)
        
        return['FINISHED']





## Operators ## 

# # Create Board Operator
class OP_create_board(bpy.types.Operator):
    bl_idname = 'woodcut.create_board'
    bl_label = 'Criar chapa'
    bl_options = {'REGISTER', 'UNDO'}    


# # # Show OP only in View_3d context
    @classmethod
    def poll(cls, context):         
        return context.area.type == 'VIEW_3D'

# # # Create Object
    def execute(self, context):
        woodcut = bpy.context.scene.woodcut
        bpy.ops.object.select_all(action = 'DESELECT')
        
        board_width = WC_methods.F_thickness(WC_methods)
        
        pos_x = bpy.context.scene.cursor.location[0]
        pos_y = bpy.context.scene.cursor.location[1]
        pos_z = bpy.context.scene.cursor.location[2] + (board_width / 2)
        
        WC_methods.F_create_board(WC_methods, board_width,
                                  woodcut.board_height,
                                  woodcut.board_lenght,
                                  pos_x, pos_y, pos_z, 0, 0, 0
                                  ,'Chapa')
                                  
# # # # Create Collection
        if bpy.data.collections.find('WoodCut') == -1:
            coll = bpy.data.collections.new('WoodCut')
            bpy.context.scene.collection.children.link(coll)
                        
        else: 
            coll = bpy.data.collections['WoodCut']
        
        wood_board = bpy.context.object
        c_woodcut = bpy.data.collections['WoodCut']
        c_current = wood_board.users_collection[0]
        if c_current != bpy.data.collections['WoodCut']:
            c_woodcut.objects.link(wood_board)
            c_current.objects.unlink(wood_board)
        
        return{'FINISHED'}
   

# # # Create Box Operator
class OP_create_box(bpy.types.Operator):
    bl_idname = 'woodcut.create_box'
    bl_label = 'Criar caixa'
    bl_options = {'REGISTER', 'UNDO'}    

# # # Show OP only in View_3d context
    @classmethod
    def poll(cls, context):         
        return context.area.type == 'VIEW_3D'

# # # Create Object
    def execute(self, context):
        woodcut = bpy.context.scene.woodcut
        bpy.ops.object.select_all(action = 'DESELECT')

        height = woodcut.box_height
        lenght = woodcut.box_lenght
        width = woodcut.box_width
        c_x = bpy.context.scene.cursor.location[0]
        c_y = bpy.context.scene.cursor.location[1]
        c_z = bpy.context.scene.cursor.location[2]
        box_boards = []
        
        b_type = woodcut.box_type
        t_backboard = woodcut.backboard
        board_width = WC_methods.F_thickness(WC_methods)
        n_part = woodcut.box_partitions
        
        back_t = 0.006
        f_size = 0.045
        t_size = 0.05
        e_size = 0.07
        
        if woodcut.build_support == True:
            sup_s = 0.1
        else: sup_s = 0
        
# # # # Create support
        if woodcut.build_support == True:
            WC_methods.F_create_board(WC_methods, board_width, lenght, sup_s,               
                                      c_x,
                                      c_y - (width / 2) + 0.05 + (board_width / 2),
                                      c_z + (sup_s / 2),
                                      radians(90), 0, 0, 'Suporte')
            box_boards.append(bpy.context.object)
            
            WC_methods.F_create_board(WC_methods, board_width, lenght, sup_s,               
                                      c_x,
                                      c_y + (width / 2) - 0.02 - (board_width / 2),
                                      c_z + (sup_s / 2),
                                      radians(90), 0, 0, 'Suporte')
            box_boards.append(bpy.context.object)
            
            WC_methods.F_create_board(WC_methods, 
                                      board_width,
                                      width - (board_width * 2) - 0.07,
                                      sup_s,               
                                      c_x + (lenght / 2) - (board_width / 2),
                                      c_y + (board_width * 2) - board_width,
                                      c_z + (sup_s / 2),
                                      radians(90), 0, radians(90), 'Suporte')
            box_boards.append(bpy.context.object)
                             
            WC_methods.F_create_board(WC_methods, 
                                      board_width,
                                      width - (board_width * 2) - 0.07,
                                      sup_s,               
                                      c_x - (lenght / 2) + (board_width / 2),
                                      c_y + (board_width * 2) - board_width,
                                      c_z + (sup_s / 2),
                                      radians(90), 0, radians(90), 'Suporte')
            box_boards.append(bpy.context.object)


# # # # Create Base
        if ((t_backboard == 'fora') and ((b_type == 'basico') or (b_type == 'balcao'))):
            WC_methods.F_create_board(WC_methods, board_width, lenght, width - back_t,
                                  c_x,
                                  c_y + (back_t / 2),
                                  c_z + (board_width / 2) + sup_s,
                                  0, 0, 0, 'Base / Chapeu')
            box_boards.append(bpy.context.object)
            
            if b_type == 'basico':
                WC_methods.F_create_board(WC_methods, board_width, lenght, width - back_t,
                                  c_x,
                                  c_y + (back_t / 2),
                                  c_z + height - (board_width / 2),
                                  0, 0, 0, 'Base / Chapeu')
            box_boards.append(bpy.context.object)
            
        else:
            WC_methods.F_create_board(WC_methods, board_width, lenght, width,
                                  c_x,
                                  c_y,
                                  c_z + (board_width / 2) + sup_s,
                                  0, 0, 0, 'Base / Chapeu')
            box_boards.append(bpy.context.object)
            
            if b_type == 'basico':
                WC_methods.F_create_board(WC_methods, board_width, lenght, width ,
                                  c_x,
                                  c_y,
                                  c_z + height - (board_width / 2),
                                  0, 0, 0, 'Base / Chapeu')
            box_boards.append(bpy.context.object)

# # # # Create Fianco
        if ((t_backboard == 'fora') and ((b_type == 'basico') or (b_type == 'balcao'))):
            WC_methods.F_create_board(WC_methods,
                                     board_width, height - (board_width * 2) - sup_s, width - back_t,
                                     c_x - (lenght / 2) + (board_width / 2),
                                     c_y + (back_t / 2),
                                     c_z + (height / 2) + (sup_s / 2),
                                     0, radians(90), 0, 'Fianco')
            box_boards.append(bpy.context.object)
            
            WC_methods.F_create_board(WC_methods,
                                     board_width, height - (board_width * 2) - sup_s, width - back_t,
                                     c_x + (lenght / 2) - (board_width / 2),
                                     c_y + (back_t / 2),
                                     c_z + (height / 2) + (sup_s / 2),
                                     0, radians(90), 0, 'Fianco')
            box_boards.append(bpy.context.object)
            
        elif b_type == 'cozinha':
            WC_methods.F_create_board(WC_methods,
                                     board_width, height - (board_width * 2) - f_size - sup_s, width,
                                     c_x - (lenght / 2) + (board_width / 2),
                                     c_y,
                                     c_z + (height / 2) - (f_size / 2) + (sup_s / 2),
                                     0, radians(90), 0, 'Fianco')
            box_boards.append(bpy.context.object)
            
            WC_methods.F_create_board(WC_methods,
                                     board_width, height - (board_width * 2) - f_size - sup_s, width,
                                     c_x + (lenght / 2) - (board_width / 2),
                                     c_y,
                                     c_z + (height / 2) - (f_size / 2) + (sup_s / 2),
                                     0, radians(90), 0, 'Fianco')
            box_boards.append(bpy.context.object)
        
        else:
            WC_methods.F_create_board(WC_methods,
                                     board_width, height - (board_width * 2) - sup_s, width, 
                                     c_x - (lenght / 2) + (board_width / 2),
                                     c_y,
                                     c_z + (height / 2) + (sup_s / 2),
                                     0, radians(90), 0, 'Fianco')
            box_boards.append(bpy.context.object)
            
            WC_methods.F_create_board(WC_methods,
                                     board_width, height - (board_width * 2) - sup_s, width,
                                     c_x + (lenght / 2) - (board_width / 2),
                                     c_y,
                                     c_z + (height / 2) + (sup_s / 2),
                                     0, radians(90), 0, 'Fianco')
            box_boards.append(bpy.context.object)
 
 # # # # Create Partitions
        if n_part > 1:
            if ((t_backboard == 'fora') and (b_type == 'basico' or b_type == 'balcao')):
                WC_methods.F_create_board(WC_methods, board_width,
                                         height - (board_width * 2) - sup_s,
                                         width - back_t,
                                         c_x + (board_width / 2),
                                         c_y + (back_t / 2),
                                         c_z + (height / 2) + (sup_s / 2),
                                         0, radians(90), 0,
                                         'Fianco Engrossado')
                box_boards.append(bpy.context.object)
                
                WC_methods.F_create_board(WC_methods,
                                         board_width,
                                         height - (board_width * 2) - sup_s,
                                         e_size,
                                         c_x - (board_width / 2),
                                         c_y - (width / 2) + (e_size / 2),
                                         c_z + (height / 2) + (sup_s / 2),
                                         0, radians(90), 0,
                                         'Fianco Engrosso')
                box_boards.append(bpy.context.object)
                
            if ((t_backboard == 'recuado') and (b_type == 'basico' or b_type == 'balcao')):
                WC_methods.F_create_board(WC_methods,
                                         board_width,
                                         height - (board_width * 2) - sup_s,
                                         width - back_t - 0.015,
                                         c_x + (board_width / 2),
                                         c_y + ((back_t + 0.015) / 2) - 0.015 - back_t,
                                         c_z + (height / 2) + (sup_s / 2),
                                         0, radians(90), 0,
                                         'Fianco Engrossado')
                box_boards.append(bpy.context.object)
                
                WC_methods.F_create_board(WC_methods,
                                         board_width,
                                         height - (board_width * 2) - sup_s,
                                         e_size,
                                         c_x - (board_width / 2),
                                         c_y - (width / 2) + (e_size / 2),
                                         c_z + (height / 2) + (sup_s / 2),
                                         0, radians(90), 0,
                                         'Fianco Engrosso')
                box_boards.append(bpy.context.object)
                
            if ((t_backboard == 'aberto') and (b_type == 'basico' or b_type == 'balcao')):
                WC_methods.F_create_board(WC_methods,
                                         board_width,
                                         height - (board_width * 2) - sup_s,
                                         width,
                                         c_x + (board_width / 2),
                                         c_y,
                                         c_z + (height / 2) + (sup_s / 2),
                                         0, radians(90), 0,
                                         'Fianco Engrossado')
                box_boards.append(bpy.context.object)
                
                WC_methods.F_create_board(WC_methods,
                                         board_width,
                                         height - (board_width * 2) - sup_s,
                                         e_size,
                                         c_x - (board_width / 2),
                                         c_y - (width / 2) + (e_size / 2),
                                         c_z + (height / 2) + (sup_s / 2),
                                         0, radians(90), 0,
                                         'Fianco Engrosso')
                box_boards.append(bpy.context.object)
                
            if b_type == 'cozinha':
                WC_methods.F_create_board(WC_methods,
                                         board_width,
                                         height - (board_width * 2) - f_size - sup_s,
                                         width,
                                         c_x + (board_width / 2),
                                         c_y,
                                         c_z + board_width + ((height - (board_width * 2) - f_size + sup_s) / 2),
                                         0, radians(90), 0,
                                         'Fianco Engrossado')
                box_boards.append(bpy.context.object)
                
                WC_methods.F_create_board(WC_methods,
                                         board_width,
                                         height - (board_width * 2) - f_size - sup_s,
                                         e_size,
                                         c_x - (board_width / 2),
                                         c_y - (width / 2) + (e_size / 2),
                                         c_z + board_width + ((height - (board_width * 2) - f_size + sup_s) / 2),
                                         0, radians(90), 0,
                                         'Fianco Engrosso')
                box_boards.append(bpy.context.object)
                
                WC_methods.F_create_board(WC_methods,
                                      board_width, height - (board_width * 2) - f_size - sup_s, t_size,               
                                      c_x + board_width + (t_size / 2),
                                      c_y - (board_width / 2) + (width / 2),
                                      c_z + (height / 2) - (f_size / 2) + (sup_s / 2),
                                      0, radians(90), radians(90), 'Montante')
                box_boards.append(bpy.context.object)
 
 # # # # Create Travessa
        if b_type == 'balcao':
            WC_methods.F_create_board(WC_methods, board_width, lenght, t_size,              
                                      c_x,
                                      c_y + (width / 2) - (t_size / 2),
                                      c_z + height - (board_width / 2),
                                      0, 0, 0, 'Travessa')
            box_boards.append(bpy.context.object)
            
            WC_methods.F_create_board(WC_methods, board_width, lenght, t_size,              
                                      c_x,
                                      c_y - (width / 2) + (t_size / 2),
                                      c_z + height - (board_width / 2),
                                      0, 0, 0, 'Travessa')
            box_boards.append(bpy.context.object)
                                      
        elif b_type == 'cozinha':
            WC_methods.F_create_board(WC_methods, board_width, lenght, t_size,              
                                      c_x,
                                      c_y - (width / 2) + (t_size / 2),
                                      c_z + height - (board_width / 2) - f_size,
                                      0, 0, 0, 'Travessa')
            box_boards.append(bpy.context.object)
            
# # # # Create Montante
        if b_type == 'cozinha':
            WC_methods.F_create_board(WC_methods,
                                      board_width, height - (board_width * 2) - f_size - sup_s, t_size,               
                                      c_x + (lenght / 2) - (t_size / 2) - board_width,
                                      c_y - (board_width / 2) + (width / 2),
                                      c_z + (height / 2) - (f_size / 2) + (sup_s / 2),
                                      0, radians(90), radians(90), 'Montante')
            box_boards.append(bpy.context.object)
                
            WC_methods.F_create_board(WC_methods,
                                      board_width, height - (board_width * 2) - f_size - sup_s, t_size,               
                                      c_x - (lenght / 2) + (t_size / 2) + board_width,
                                      c_y - (board_width / 2) + (width / 2),
                                      c_z + (height / 2) - (f_size / 2) + (sup_s / 2),
                                      0, radians(90), radians(90), 'Montante')
            box_boards.append(bpy.context.object)
            
# # # # Create Front
        if bpy.context.scene.woodcut.box_type == 'cozinha':
            WC_methods.F_create_board(WC_methods, board_width, lenght, f_size,                
                                      c_x,
                                      c_y - (width / 2) + (board_width / 2),
                                      c_z + height - (f_size / 2),
                                      radians(90), 0, 0, 'Frontal') 
            box_boards.append(bpy.context.object)
            
# # # # Create BackBoard
        if ((t_backboard == 'fora') and (b_type == 'basico' or b_type == 'balcao')):
            if lenght > height:
                WC_methods.F_create_board(WC_methods, back_t, lenght, height - sup_s,               
                                          c_x,
                                          c_y + (width / 2) + (back_t / 2),
                                          c_z + (height / 2) + (sup_s / 2),
                                          radians(90), 0, 0, 'Fundo')
                box_boards.append(bpy.context.object)

            else:
                WC_methods.F_create_board(WC_methods, back_t, height - sup_s, lenght,               
                                          c_x,
                                          c_y + (width / 2) + (back_t / 2),
                                          c_z + (height / 2) + (sup_s / 2),
                                          radians(90), radians(90), 0, 'Fundo')
                box_boards.append(bpy.context.object)
        
        if ((t_backboard == 'recuado') and (b_type == 'basico' or b_type == 'balcao')):
            if lenght > height:
                WC_methods.F_create_board(WC_methods,
                                          back_t,
                                          lenght - (board_width * 2) + 0.015,
                                          height - (board_width * 2) + 0.015 - sup_s,
                                          c_x,
                                          c_y + width / 2 - (back_t / 2) - 0.015,
                                          c_z + (height / 2) + (sup_s / 2),
                                          radians(90), 0, 0, 'Fundo')
                box_boards.append(bpy.context.object)
            
            else:
                WC_methods.F_create_board(WC_methods,
                                          back_t,
                                          height - (board_width * 2) + 0.015 - sup_s,
                                          lenght - (board_width * 2) + 0.015,
                                          c_x,
                                          c_y + width / 2 - (back_t / 2) - 0.015,
                                          c_z + (height / 2) + (sup_s / 2),
                                          radians(90), radians(90), 0, 'Fundo')
                box_boards.append(bpy.context.object)


# # # Create Collection and Add                                                                         
        if bpy.data.collections.find('WoodCut') == -1:
            w_coll = bpy.data.collections.new('WoodCut')
            bpy.context.scene.collection.children.link(w_coll)
            
        else:
            w_coll = bpy.data.collections['WoodCut']
            
        if woodcut.box_type == 'basico':
            coll_name = 'Caixa'
        elif woodcut.box_type == 'balcao':
            coll_name = 'Balcao'
        elif woodcut.box_type == 'cozinha':
            coll_name = 'Balcao Cozinha'
                        

        coll = bpy.data.collections.new(coll_name)
        bpy.data.collections['WoodCut'].children.link(coll)
        
        for box in box_boards:
            box.select_set(state = True)
        
        for ob in bpy.context.selected_objects:
            c_coll = box.users_collection[0]
            coll.objects.link(ob)
            c_coll.objects.unlink(ob)
        
        return{'FINISHED'}    
    


# # Calculate Operator
class OP_create_list(bpy.types.Operator):
    bl_idname = 'woodcut.create_list'
    bl_label = 'Calcular projeto'
    
# # # Show OP only in View_3d context
    @classmethod
    def poll(cls, context):
        return context.area.type == 'VIEW_3D'

# # # Generate Project 
    def execute(self, context):
        
        if bpy.data.collections.find('WoodCut') != -1:
        
            WC_methods.F_clear_lists(WC_methods)
            woodcut = bpy.context.scene.woodcut
            objects = []
            
            bpy.ops.object.select_all(action = 'DESELECT')
            all_objects = bpy.data.collections['WoodCut'].all_objects
            
            for ob in all_objects:
                if ob.type == 'MESH':
                    objects.append(ob)
         
# # # # Check avalible Size
            checked = True 
            for ob in objects:
                
# # # # # Apply Scale center Pivot
                bpy.data.objects[ob.name].select_set(True)
                bpy.context.view_layer.objects.active = bpy.data.objects[ob.name]
                
                # ERROR - Moving Pieces
                #try:
                #    bpy.ops.object.transform_apply(scale = True, rotation = False, location = False)
                #except:
                #    pass
                #bpy.ops.object.origin_set(type = 'ORIGIN_GEOMETRY', center = 'MEDIAN')
                
                bpy.ops.object.select_all(action = 'DESELECT')
                
# # # # # Check
                dim_x = round(ob.dimensions.x, 3)
                dim_y = round(ob.dimensions.y, 3)
                dim_z = round(ob.dimensions.z, 3)
            
                if dim_z in [0.006, 0.015, 0.018]:
                    pass
                else:
                    checked = False
                    self.report({'ERROR'}, ob.users_collection[0].name + ': ' + ob.name + ' com espessura inválida: ' + str(dim_z) + ('m'))
                    break
            
                if dim_x > 2.75:
                    checked = False
                    self.report({'ERROR'}, ob.users_collection[0].name + ': ' + ob.name + ' com altura inválida: ' + str(dim_x) + ('m'))
                    break
            
                if dim_y > 1.84:
                    checked = False
                    self.report({'ERROR'}, ob.users_collection[0].name + ': ' + ob.name + ' com largura inválida: ' + str(dim_y) + ('m'))
                    break
        
# # # # Checked OK!
            if checked == True:
                
# # # # # Fix names
                for ob in objects:
                    if '.' in ob.name:
                        ob_name = ob.name[0: ob.name.index('.')]
                    else:
                        ob_name = ob.name
                    
# # # # # Create obj settings list 
                    d_ob = [ob_name, # 0 - Nome
                            ob.material_slots[0].name, # 1 - Material
                            ob.users_collection[0].name, # 2 - Collection
                            round(ob.dimensions.x, 3), # 3 - Dim x
                            round(ob.dimensions.y, 3), # 4 - Dim y
                            round(ob.dimensions.z, 3), # 5 - Dim z
                            1, # 6 - counter
                            1] # 7 - id
                    woodcut.l_parts.append(d_ob)
                    
# # # # # List Shaders
                    if d_ob[5] == 0.006:
                        d_ob[1] += ' 6'
                    
                    if d_ob[5] == 0.015:
                        d_ob[1] += ' 15'
                        
                    if d_ob[5] == 0.018:
                        d_ob[1] += ' 18'
                    
                    if d_ob[1] in woodcut.l_shaders:
                        pass
                    else:
                        woodcut.l_shaders.append(d_ob[1])
                    
# # # # # List Boxes               
                    if ob.users_collection[0].name in woodcut.l_boxes:
                        pass
                    else: 
                        bpy.context.scene.woodcut.l_boxes.append(ob.users_collection[0].name)
                        
# # # # #  Remove Duplicates transfer to CutList generate ID
                woodcut.l_cutlist.append([''])
                idx = 1
                for i in range(len(woodcut.l_parts)):
                    add_ob = True
                     
                    for u in range(len(woodcut.l_cutlist)):
                        if woodcut.l_parts[i][0] == woodcut.l_cutlist[u][0]:
                            if woodcut.l_parts[i][1] == woodcut.l_cutlist[u][1]:
                                if woodcut.l_parts[i][3] == woodcut.l_cutlist[u][3]:
                                    if woodcut.l_parts[i][4] == woodcut.l_cutlist[u][4]:
                                         if woodcut.l_parts[i][5] == woodcut.l_cutlist[u][5]:
                                            add_ob = False
                                            woodcut.l_cutlist[u][6] += 1
                                            woodcut.l_parts[i][7] = woodcut.l_cutlist[u][7]
                                            
                    if add_ob == True:
                        woodcut.l_cutlist.append(woodcut.l_parts[i][:])
                        woodcut.l_cutlist[idx][7] = idx
                        woodcut.l_parts[i][7] = idx
                        idx += 1
                
                del woodcut.l_cutlist[0]
                
# # # # # Transfer to Build List
                woodcut.l_buildlist.append([''])
                for i in range(len(woodcut.l_parts)):
                    add_ob = True
                    
                    for u in range(len(woodcut.l_buildlist)):
                        if woodcut.l_parts[i][0] == woodcut.l_buildlist[u][0]:
                            if woodcut.l_parts[i][1] == woodcut.l_buildlist[u][1]:
                                if woodcut.l_parts[i][2] == woodcut.l_buildlist[u][2]:
                                    if woodcut.l_parts[i][3] == woodcut.l_buildlist[u][3]:
                                        if woodcut.l_parts[i][4] == woodcut.l_buildlist[u][4]:
                                            if woodcut.l_parts[i][5] == woodcut.l_buildlist[u][5]:
                                                add_ob = False
                                                woodcut.l_buildlist[u][6] += 1
                                                
                    if add_ob == True:
                        woodcut.l_buildlist.append(woodcut.l_parts[i][:])
                        
                del woodcut.l_buildlist[0]
                
# # # # # Area by material
                for shader in woodcut.l_shaders:
                    woodcut.l_boards.append(0)
                    
                for d_ob in woodcut.l_parts:
                    idx = woodcut.l_shaders.index(d_ob[1])
                    b_area = d_ob[3] * d_ob[4]
                    b_est = b_area / 5.06                    
                    woodcut.l_boards[idx] = round(woodcut.l_boards[idx] + b_est, 2) 
                
# # # # # Visual Map
                if woodcut.build_visual_map == True:

# # # # # # Create id Collection and Shader
                    if bpy.data.materials.find('wc_id') == -1:
                        t_coll = bpy.data.collections.new('ID')
                        bpy.data.collections['WoodCut'].children.link(t_coll)
                        s_text = bpy.data.materials.new(name = 'wc_id')
                        s_text.use_nodes = True
                        s_text.node_tree.nodes['Principled BSDF'].inputs[17].default_value = [1, 0, 0, 1]
                        s_text.node_tree.nodes['Principled BSDF'].inputs[0].default_value = [1, 0, 0, 1]
                     
                     
                    if bpy.data.collections.find('ID') == -1:
                        id_coll = bpy.data.collections.new('ID')
                        bpy.data.collections['WoodCut'].children.link(id_coll)
                    
                    else:
                         id_coll = bpy.data.collections['ID']
                    
# # # # # # Create id objects                    
                    bpy.ops.object.select_all(action = 'DESELECT')
                    all_objects = bpy.data.collections['WoodCut'].all_objects
                    l_obj = []
                    for obj in all_objects:
                        if obj.type == 'MESH':
                            l_obj.append(obj)
                    
                    len_obj = len(l_obj)
                    t_idx = 0
                    for i in range(len_obj):
                        t_loc = (l_obj[i].location[0] - 0.01, l_obj[i].location[1] - 0.01, l_obj[i].location[2] + 0.01)
                        t_rot = (radians(90), 0, l_obj[i].rotation_euler[0] + radians(180))
                        bpy.ops.object.text_add(radius = 0.1, location = t_loc, rotation = t_rot)
                
                        o_text = bpy.context.object
                        o_text.data.body = ('.' + str(woodcut.l_parts[t_idx][7]))
                        o_text.name = str(woodcut.l_parts[t_idx][0])
                        o_text.hide_render = True
            
                        tc_coll = o_text.users_collection[0]
                        id_coll.objects.link(o_text)
                        tc_coll.objects.unlink(o_text) 
                    
                        bpy.context.object.active_material = bpy.data.materials['wc_id']
                
                        t_idx += 1
                    
        bpy.ops.object.select_all(action = 'DESELECT')

        return{'FINISHED'}


# # Clear List Operator
class OP_clear_list(bpy.types.Operator):
    bl_idname = 'woodcut.clear_list'
    bl_label = 'Limpar calculo'

# # # Show OP only in View_3d context
    @classmethod
    def poll(cls, context):
        return context.area.type == 'VIEW_3D'
    
    def execute(self, context):
        WC_methods.F_clear_lists(WC_methods)

        return{'FINISHED'}
    

# # Export List Operator
class OP_export_list(bpy.types.Operator):
    bl_idname = 'woodcut.export_list'
    bl_label = 'Exportar Listas'
    
# # # Show OP only in View_3d context
    @classmethod
    def poll(cls, context):
        return context.area.type == 'VIEW_3D'
    
# # # Export
    def execute(self, context):
        
        woodcut = bpy.context.scene.woodcut
        p_name = bpy.path.basename(bpy.context.blend_data.filepath)
        export_text_c = ''
        export_text_b = ''
        export_text_l = ''

# # # CUT LIST 
        if ((woodcut.export_type == 'todos') or (woodcut.export_type == 'corte')):
            
# # # # Sheet Title
            export_text_c += '''
            
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
    
    <meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
    <title></title>
    <meta name="generator" content="LibreOffice 6.4.0.3 (Windows)"/>
    <meta name="created" content="2021-04-16T16:18:25.564000000"/>
    <meta name="changed" content="2021-04-17T17:46:28.005000000"/>
    
    <style type="text/css">
        body,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Liberation Sans"; font-size:x-small }
        a.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } 
        a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } 
        comment { display:none;  } 
    </style>
    
</head>

<body>
<table cellspacing="0" border="0">
    <colgroup width="27"></colgroup>
    <colgroup width="57"></colgroup>
    <colgroup width="71"></colgroup>
    <colgroup width="85"></colgroup>
    <colgroup width="57"></colgroup>
    <colgroup width="420"></colgroup>
    <<tr>
        <td style="border-bottom: 1px solid #000000" colspan=7 height="33" align="center" valign=middle><b><font size=5>''' + p_name[0: p_name.index('.')] + '''</font></b></td>
        </tr>
    <tr>
        <td colspan=7 height="25" align="center" valign=middle><font size=4>Plano de corte</font></td>
        </tr>
        <tr>
        <td colspan=7 height="17" align="left"><br></td>
        </tr>
'''
            
# # # # Sort Cut List
            woodcut.l_cutlist = sorted(woodcut.l_cutlist, key = itemgetter(3, 4))
            woodcut.l_cutlist.reverse()                      
            
# # # # Create table by material and thickness                 
            for shader in woodcut.l_shaders:
                exp_s = False
                for part in woodcut.l_cutlist:
                    if part[1] == shader:
                        exp_s = True
                    
                if exp_s == True:
                    export_text_c += '''
                        
    <tr>
        <td colspan=7 height="17" align="left"><br></td>
        </tr>
    <tr>
    <tr>
        <td colspan=7 height="17" align="left"><br></td>
        </tr>
    <tr>
        <td style="border-top: 1px solid #000000" colspan=7 height="25" align="left" valign=middle><b><font size=5>   ''' + shader + '''</font></b></td>
        </tr>
    <tr>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="20" align="center" valign=middle bgcolor="#000000"><b><font size=3><br></font></b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font size=3>Qnt.</font></b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font size=3>Altura</font></b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font size=3>Largura</font></b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font size=3>Id</font></b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font size=3>Nome</font></b></td>
    </tr>
    '''
                    for part in woodcut.l_cutlist:
                        if part[1] == shader:
                            export_text_c += '''
                                
<tr>
        <td style="border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="24" align="center" valign=middle><font size=2><br></font></td>
        <td style="border-bottom: 1px solid #000000" align="center" valign=middle sdval="10" sdnum="1046;"><font size=2>''' + str(part[6]) + '''</font></td>
        <td style="border-bottom: 1px solid #000000" align="center" valign=middle><font size=2>''' + str(round(part[3]*100 ,1)) + '''</font></td>
        <td style="border-bottom: 1px solid #000000" align="center" valign=middle><font size=2>''' + str(round(part[4]*100 ,1)) + '''</font></td>
        <td style="border-bottom: 1px solid #000000" align="center" valign=middle sdval="10" sdnum="1046;"><font size=2>''' + str(part[7]) + '''</font></td>
        <td style="border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font size=2>''' + part[0] + '''</font></td>
    </tr>
    '''
    
            export_text_c += '''
</table>
<!-- ************************************************************************** -->
</body>

</html>'''

# # # # # Save File
            f_name = p_name[0: p_name.index('.')] + '_CutList.html'
            save_folder = woodcut.export_path
            filename = os.path.join(save_folder, f_name)
            os.makedirs(os.path.dirname(filename), exist_ok = True)
            file = open(filename, 'w')
            file.write(export_text_c)
            file.close
                        
            
# # # BUILD LIST
        if ((woodcut.export_type == 'todos') or (woodcut.export_type == 'montagem')):

# # # # Sheet Title
            export_text_b += '''
            
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
    
    <meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
    <title></title>
    <meta name="generator" content="LibreOffice 6.4.0.3 (Windows)"/>
    <meta name="created" content="2021-04-16T16:18:25.564000000"/>
    <meta name="changed" content="2021-04-17T17:46:28.005000000"/>
    
    <style type="text/css">
        body,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Liberation Sans"; font-size:x-small }
        a.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } 
        a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } 
        comment { display:none;  } 
    </style>
    
</head>

<body>
<table cellspacing="0" border="0">
    <colgroup width="27"></colgroup>
    <colgroup span="2" width="57"></colgroup>
    <colgroup width="285"></colgroup>
    <colgroup width="135"></colgroup>
    <colgroup width="71"></colgroup>
    <colgroup width="85"></colgroup>
    <tr>
        <td style="border-bottom: 1px solid #000000" colspan=7 height="33" align="center" valign=middle><b><font size=5>''' + p_name[0: p_name.index('.')] + '''</font></b></td>
        </tr>
    <tr>
        <td colspan=7 height="25" align="center" valign=middle><font size=4>Lista de montagem</font></td>
        </tr>
    <tr>
        <td colspan=7 height="17" align="left"><br></td>
        </tr>
 '''
            
# # # # Sort But List
            woodcut.l_buildlist = sorted(woodcut.l_buildlist, key = itemgetter(0, 7))
            
# # # # Create table by material and thickness
            for box in woodcut.l_boxes:
                export_text_b += '''
                
    <tr>
        <td colspan=7 height="17" align="left"><br></td>
        </tr>
    <tr>
    <tr>
        <td colspan=7 height="17" align="left"><br></td>
        </tr>
    <tr>
        <td style="border-top: 1px solid #000000" colspan=7 height="25" align="left" valign=middle><b><font size=5>   ''' + box + '''</font></b></td>
        </tr>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="20" align="center" valign=middle bgcolor="#000000"><b><font size=3><br></font></b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font size=3>Id</font></b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font size=3>Qnt.</font></b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font size=3>Nome</font></b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font size=3>Material</font></b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font size=3>Altura</font></b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font size=3>Largura</font></b></td>
    </tr>
    '''
                for part in woodcut.l_buildlist:
                    if (part[2] == box):
                        export_text_b += '''
                        
    <tr>
        <td style="border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="24" align="center" valign=middle><font size=2><br></font></td>
        <td style="border-bottom: 1px solid #000000" align="center" valign=middle sdval="10" sdnum="1046;"><font size=2>''' + str(part[7]) + '''</font></td>
        <td style="border-bottom: 1px solid #000000" align="center" valign=middle sdval="10" sdnum="1046;"><font size=2>''' + str(part[6]) + '''</font></td>
        <td style="border-bottom: 1px solid #000000" align="center" valign=middle><font size=2>''' + part[0] + '''</font></td>
        <td style="border-bottom: 1px solid #000000" align="center" valign=middle><font size=2>''' + part[1] + '''</font></td>
        <td style="border-bottom: 1px solid #000000" align="center" valign=middle><font size=2>''' + str(round(part[3]*100 ,1)) + '''</font></td>
        <td style="border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font size=2>''' + str(round(part[4]*100 ,1)) + '''</font></td>
    </tr>
                                
'''            

            export_text_b += '''
</table>
<!-- ************************************************************************** -->
</body>

</html>'''

# # # # # Save File
            f_name = p_name[0: p_name.index('.')] + '_BuildList.html'
            save_folder = woodcut.export_path
            filename = os.path.join(save_folder, f_name)
            os.makedirs(os.path.dirname(filename), exist_ok = True)
            file = open(filename, 'w')
            file.write(export_text_b)
            file.close

# # # CSV
        if ((woodcut.export_type == 'todos') or (woodcut.export_type == 'csv')):
            
# # # # Sort Part List
            woodcut.l_parts = sorted(woodcut.l_parts, key = itemgetter(1, 7))

# # # # Export 
            export_text_l = 'ID, Nome, Altura, Largura, Espessura, Material, Bloco, Projeto \n'
                        
            for part in woodcut.l_parts:
                export_text_l += str(part[7]) + ',' + part[0] + ',' + str(part[3]*100) + ',' + str(part[4]*100) + ',' + str(part[5]) + ',' + part[1] + ',' + part[2] + ',' + p_name[0: p_name.index('.')] + ' \n'
                
            f_name = p_name[0: p_name.index('.')] + '_list.csv'
            save_folder = woodcut.export_path
            filename = os.path.join(save_folder, f_name)
            os.makedirs(os.path.dirname(filename), exist_ok = True)
            file = open(filename, 'w')
            file.write(export_text_l)
            file.close
            
        return{'FINISHED'}





# Panels #

# # Create Main Panel 
class L_PT_main(bpy.types.Panel):
    bl_label = "Construir"
    bl_idname = "L_PT_main"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "WoodCut"

    def draw(self, context):
        pass

# # Create Board Panel
class L_PT_create_board(bpy.types.Panel):
    bl_label = "Chapa"
    bl_idname = "L_PT_create_board"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "WoodCut"
    bl_parent_id = "L_PT_main"
    
    def draw(self, context):
        woodcut = bpy.context.scene.woodcut
    
        row = self.layout.row()
        row.prop(woodcut, 'board_height')
        row = self.layout.row()
        row.prop(woodcut, 'board_lenght')
        row = self.layout.row()
        row.prop(woodcut, 'thickness_board')
        row = self.layout.row()
        row.operator('woodcut.create_board', icon = 'MESH_PLANE')
        
# # Create Box Panel
class L_PT_create_box(bpy.types.Panel):
    bl_label = "Caixa"
    bl_idname = "L_PT_create_box"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "WoodCut"
    bl_parent_id = "L_PT_main"
    
    def draw(self, context):
        woodcut = bpy.context.scene.woodcut
        
        row = self.layout.row()
        row.prop(woodcut, 'box_type')
        
        if  ((woodcut.box_type == 'basico') or
            (woodcut.box_type == 'balcao')):
            row = self.layout.row()
            row.prop(woodcut, 'backboard')
    
        row = self.layout.row()
        row.prop(woodcut, 'thickness_board')
        row = self.layout.row()
        row.prop(woodcut, 'box_height')
        row = self.layout.row()
        row.prop(woodcut, 'box_lenght')
        row = self.layout.row()
        row.prop(woodcut, 'box_width')
        row = self.layout.row()
        row.prop(woodcut, 'box_partitions')
        row = self.layout.row()
        row.prop(woodcut, 'build_support')
        row = self.layout.row()
        row.operator('woodcut.create_box', icon = 'CUBE')

        
# # Calculate Panel
class PT_cutlist(bpy.types.Panel):
    bl_label = 'Calcular'
    bl_idname = 'L_PT_cut_list'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'WoodCut'
    
    def draw(self, context):
        woodcut = bpy.context.scene.woodcut
        
        row = self.layout.row()
        row.prop(woodcut, 'build_visual_map')
        row = self.layout.row()
        row.operator('woodcut.create_list', icon = 'COLLAPSEMENU')
        row = self.layout.row()
        row.operator('woodcut.clear_list', icon = 'CANCEL')

# # Create List Info Panel
class L_PT_list_info(bpy.types.Panel):
    bl_label = 'Informações'
    bl_idname = 'L_PT_list_info'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'WoodCut'
    bl_parent_id = "L_PT_cut_list"
    
    def draw(self, context):
        woodcut = bpy.context.scene.woodcut
        
        row = self.layout.row()
        row.label(text = ('Partes: ') + str(len(woodcut.l_parts)))
        row.label(text = ('Caixas: ') + str(len(woodcut.l_boxes)))
        row = self.layout.row()
        row.label(text = ('Materiais: '))
        row.label(text = ('Chapas: '))
        
        if len(woodcut.l_shaders) > 0:
            for i in range(len(woodcut.l_shaders)):
                row = self.layout.row()
                row.label(text = woodcut.l_shaders[i])
                row.label(text = str(woodcut.l_boards[i]))
                

# # Create Export Panel
class L_PT_export_cutlist(bpy.types.Panel):
    bl_label = 'Exportar'
    bl_idname = 'L_PT_export_cutlist'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'WoodCut'
    
    def draw(self, context):
        woodcut = bpy.context.scene.woodcut
        
        row = self.layout.row()
        row.prop(woodcut, 'export_type')
        row = self.layout.row()
        row.prop(woodcut, 'export_path')
        row = self.layout.row()
        row.operator('woodcut.export_list', icon = 'FILE')





# Register #
classes = [OP_create_board, OP_create_box, OP_create_list,
           OP_export_list, OP_clear_list, L_PT_main,
           L_PT_create_board, L_PT_create_box, PT_cutlist,
           L_PT_list_info, L_PT_export_cutlist, PG_woodcut]

def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    bpy.types.Scene.woodcut = bpy.props.PointerProperty(type = PG_woodcut)


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
    del bpy.types.Scene.PG_woodcut
    

if __name__ == "__main__":
    register()  
